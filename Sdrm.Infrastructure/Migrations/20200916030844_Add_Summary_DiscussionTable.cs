﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sdrm.Infrastructure.Migrations
{
    public partial class Add_Summary_DiscussionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Discussions_Locations_LocationId",
                table: "Discussions");

            migrationBuilder.AlterColumn<int>(
                name: "LocationId",
                table: "Discussions",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Summary",
                table: "Discussions",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Discussions_Locations_LocationId",
                table: "Discussions",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Discussions_Locations_LocationId",
                table: "Discussions");

            migrationBuilder.DropColumn(
                name: "Summary",
                table: "Discussions");

            migrationBuilder.AlterColumn<int>(
                name: "LocationId",
                table: "Discussions",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Discussions_Locations_LocationId",
                table: "Discussions",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
