﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Sdrm.Core.Entities;
using Sdrm.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sdrm.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        #region Private Properties
        
        private readonly UserRepository _userRepository;

        private readonly DiscussionRepository _discussionRepository;

        private readonly DiscussionAttendeeRepository _discussionAttendeeRepository;

        private readonly DiscussionOutcomeRepository _discussionOutcomeRepository;

        private readonly LocationRepository _locationRepository;

        #endregion

        #region DB Sets

        public override DbSet<ApplicationUser> Users { get; set; }

        public DbSet<Discussion> Discussions { get; set; }

        public DbSet<DiscussionAttendee> DiscussionAttendees { get; set; }

        public DbSet<DiscussionOutcome> DiscussionOutcomes { get; set; }

        public DbSet<Location> Locations { get; set; }

        #endregion

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            _userRepository = new UserRepository(this);

            _discussionRepository = new DiscussionRepository(this);

            _discussionAttendeeRepository = new DiscussionAttendeeRepository(this);

            _discussionOutcomeRepository = new DiscussionOutcomeRepository(this);

            _locationRepository = new LocationRepository(this);
        }

        #region Unit of work implementation

        public UserRepository UserRepository { get { return _userRepository; } }

        public DiscussionRepository DiscussionRepository { get { return _discussionRepository; } }

        public DiscussionAttendeeRepository DiscussionAttendeeRepository { get { return _discussionAttendeeRepository; } }

        public DiscussionOutcomeRepository DiscussionOutcomeRepository { get { return _discussionOutcomeRepository; } }

        public LocationRepository LocationRepository { get { return _locationRepository; }  }

        #endregion
    }
}
