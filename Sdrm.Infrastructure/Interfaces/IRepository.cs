﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Sdrm.Infrastructure.Interfaces
{
    public interface IRepository<T> : IDisposable
    {
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
        T GetById(int id);
        IQueryable<T> GetAll();
        IQueryable<T> Search(Expression<Func<T, bool>> expression);
    }
}
