﻿using Sdrm.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sdrm.Infrastructure.Repositories
{
    public class DiscussionOutcomeRepository : BaseRepository<DiscussionOutcome>
    {
        public DiscussionOutcomeRepository(ApplicationDbContext context) : base(context)
        {

        }

        //custom methods here not available on BaseRepository
    }
}
