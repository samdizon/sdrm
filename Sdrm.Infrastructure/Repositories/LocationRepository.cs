﻿using Sdrm.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sdrm.Infrastructure.Repositories
{
    public class LocationRepository : BaseRepository<Location>
    {
        public LocationRepository(ApplicationDbContext context) : base(context)
        {
        }

        //custom methods here not available on BaseRepository
    }
}
