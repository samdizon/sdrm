﻿using Sdrm.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sdrm.Infrastructure.Repositories
{
    public class UserRepository : BaseRepository<ApplicationUser>
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {

        }

        //Custom methods not available on BaseRepository
    }
}
