﻿using Sdrm.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sdrm.Infrastructure.Repositories
{
    public class DiscussionAttendeeRepository : BaseRepository<DiscussionAttendee>
    {
        public DiscussionAttendeeRepository(ApplicationDbContext context) : base(context)
        {
        }

        //custom methods here not available on BaseRepository
    }
}
