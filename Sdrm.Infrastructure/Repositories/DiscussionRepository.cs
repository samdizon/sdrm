﻿using Sdrm.Core.Entities;

namespace Sdrm.Infrastructure.Repositories
{
    public class DiscussionRepository : BaseRepository<Discussion>
    {
        public DiscussionRepository(ApplicationDbContext context) : base(context)
        {
        }

        //Custom methods here not available on base repository
    }
}
