﻿using Microsoft.EntityFrameworkCore;
using Sdrm.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdrm.Infrastructure.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        protected DbSet<T> dbSet;
        protected DbContext dbContext;
        private bool _disposed = false;

        public BaseRepository(ApplicationDbContext context)
        {
            dbSet = context.Set<T>();
            dbContext = context;
        }

        #region  Repository Functions
        public void Insert(T entity)
        {
            dbSet.Add(entity);
            dbContext.SaveChanges();
        }

        public void Update(T entity)
        {
            dbSet.Update(entity);
            dbContext.SaveChanges();
        }

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
            dbContext.SaveChanges();
        }

        public IQueryable<T> GetAll()
        {
            return dbSet;
        }

        public T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public IQueryable<T> Search(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return dbSet.Where(expression);
        }
        #endregion

        #region Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
