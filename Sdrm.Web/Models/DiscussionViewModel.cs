﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Sdrm.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sdrm.Web.Models
{
    public class DiscussionViewModel
    {
        public int Id { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public string Subject { get; set; }

        [Required]
        public int LocationId { get; set; }

        public SelectList Locations { get; set; }

        public Guid AttendeeId { get; set; }

        public SelectList Attendee { get; set; }

        public List<DiscussionAttendee> Attendees { get; set; }

        public List<DiscussionOutcome> Outcomes { get; set; }

        [MaxLength(1000)]
        public string Summary { get; set; }
    }
}
