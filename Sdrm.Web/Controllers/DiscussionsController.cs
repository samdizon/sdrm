﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sdrm.Core.Entities;
using Sdrm.Infrastructure;
using Sdrm.Web.Models;
using static Sdrm.Web.Enums.Notification;

namespace Sdrm.Web.Controllers
{
    public class DiscussionsController : BaseController
    {
        #region Properties
        private readonly ApplicationDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;
        #endregion

        public DiscussionsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Discussions
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Discussions.Include(d => d.Location);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Discussions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discussion = await _context.Discussions
                .Include(d => d.Location)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (discussion == null)
            {
                return NotFound();
            }

            return View(discussion);
        }

        // GET: Discussions/Create
        public IActionResult Create()
        {
            ViewData["LocationId"] = new SelectList(_context.Locations, "Id", "Name");
            return View();
        }

        // POST: Discussions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Date,Subject,LocationId")] Discussion discussion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(discussion);
                
                if(await _context.SaveChangesAsync() > 0 )
                {
                    Alert("Success!", "New safety discussion is added.", AlertType.success);
                }
                else
                {
                    Alert("Error!", "Failed to save safety discussion.", AlertType.error);
                    return View(discussion);
                }


                return RedirectToAction(nameof(Edit), new { id = discussion.Id });
            }

            ViewData["LocationId"] = new SelectList(_context.Locations, "Id", "Name", discussion.LocationId);
            return View(discussion);
        }

        // GET: Discussions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discussion = await _context.Discussions.FindAsync(id);

            if (discussion == null)
            {
                return NotFound();
            }

            var locations = new SelectList(await _context.Locations.ToListAsync(), "Id", "Name");

            var administratorsInRole = await _userManager.GetUsersInRoleAsync("Administrator");

            var attendee = new SelectList(_userManager.Users.AsEnumerable()
                .Except(administratorsInRole)
                .Select(a => new
                {
                    a.Id,
                    FullName = string.Concat($"{a.Firstname} {a.MiddleName} {a.Lastname}")
                }), "Id", "FullName");

            var attendees = await _context.DiscussionAttendees.Where(a => a.DiscussionId == id).Include( a => a.Attendee).ToListAsync();

            var outcomes = await _context.DiscussionOutcomes.Where(o => o.DiscussionId == id).ToListAsync();

            var discussionsModel = new DiscussionViewModel()
            {
                Id = discussion.Id,
                Date = discussion.Date,
                Subject = discussion.Subject,
                LocationId = discussion.LocationId,
                Locations = locations,
                Attendee = attendee,
                Attendees = attendees,
                Outcomes = outcomes,
                Summary = discussion.Summary
            };

            //ViewData["LocationId"] = new SelectList(_context.Locations, "Id", "Name", discussion.LocationId);
            return View(discussionsModel);
        }

        // POST: Discussions/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Date,Subject,LocationId, Summary")] Discussion discussion)
        {
            if (id != discussion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(discussion);
                    if(await _context.SaveChangesAsync() > 0)
                    {
                        Alert("Success!", "Safety discussion is updated.", AlertType.success);
                    }
                    else
                    {
                        Alert("Error!", "Failed to update safety discussion.", AlertType.error);
                    }

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiscussionExists(discussion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Edit), new { id = discussion.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveAttendee(int id, [Bind("Id,AttendeeId")] DiscussionViewModel discussion)
        {
            if (id != discussion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var checkExistingAttendee = await _context.DiscussionAttendees.Where(a => a.AttendeeId == discussion.AttendeeId).FirstOrDefaultAsync();

                    if (checkExistingAttendee != null)
                    {
                        Alert("Error!", "Attendee is already registered", AlertType.error);
                        return RedirectToAction(nameof(Edit), new { id = discussion.Id });
                    }


                    var attendee = new DiscussionAttendee() 
                    {
                        DiscussionId = discussion.Id,
                        AttendeeId = discussion.AttendeeId
                    };
                    
                    _context.Update(attendee);
                    if(await _context.SaveChangesAsync() > 0)
                    {
                        Alert("Success!", "New attendee is added.", AlertType.success);
                    }
                    else
                    {
                        Alert("Error!", "Failed to add attendee.", AlertType.error);
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiscussionExists(discussion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Edit), new { id = discussion.Id });
            }

            return RedirectToAction(nameof(Edit), new { id = discussion.Id });
        }

        // GET: Discussions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discussion = await _context.Discussions
                .Include(d => d.Location)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (discussion == null)
            {
                return NotFound();
            }

            return View(discussion);
        }

        // POST: Discussions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var discussion = await _context.Discussions.FindAsync(id);
            _context.Discussions.Remove(discussion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteAttendee(int id)
        {
            var attendee = await _context.DiscussionAttendees.FindAsync(id);

            if (attendee == null)
            {
                Alert("Error!", "Attendee does not exist.", AlertType.error);
                return RedirectToAction(nameof(Edit), new { id = attendee.DiscussionId });
            }

            try
            {
                _context.DiscussionAttendees.Remove(attendee);
                if (await _context.SaveChangesAsync() > 0)
                {
                    Alert("Success!", "Attendee is removed successfully.", AlertType.success);
                }
            }
            catch (Exception ex)
            {

                Alert("Error!", "Attendee does not exist. Exception: " + ex.Message, AlertType.error);
            }

            return RedirectToAction(nameof(Edit), new { id = attendee.DiscussionId });
        }

        private bool DiscussionExists(int id)
        {
            return _context.Discussions.Any(e => e.Id == id);
        }
    }
}
