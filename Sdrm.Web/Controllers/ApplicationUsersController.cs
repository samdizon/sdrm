﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sdrm.Core.Entities;
using Sdrm.Infrastructure;
using static Sdrm.Web.Enums.Notification;

namespace Sdrm.Web.Controllers
{
    public class ApplicationUsersController : BaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ApplicationUsersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: ApplicationUsers
        public async Task<IActionResult> Index()
        {
            var user = _userManager.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);

            return View(await _context.Users.Where(u => u.Id != user.Id).ToListAsync());
        }

        // GET: ApplicationUsers/Details/5
        //public async Task<IActionResult> Details(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var applicationUser = await _context.Users
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (applicationUser == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(applicationUser);
        //}

        // GET: ApplicationUsers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ApplicationUsers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Firstname,MiddleName,Lastname,UserName,Email")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                //applicationUser.Id = Guid.NewGuid();
                //_context.Add(applicationUser);
                //await _context.SaveChangesAsync();

                try
                {
                    var result = await _userManager.CreateAsync(applicationUser, "Password@123");

                    if (result.Succeeded)
                    {
                        Alert("Success!", "New user saved. User can now access the application using default password.", AlertType.success);
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        Alert("Error!", "Failed to save user. Please try again.", AlertType.error);
                        return View(applicationUser);
                    }
                }
                catch (Exception ex)
                {
                    Alert("Error!", "Failed to save user. Please contact your administrator. " + ex.Message, AlertType.error);
                    return View(applicationUser); ;
                }
            }
            return View(applicationUser);
        }

        // GET: ApplicationUsers/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users.FindAsync(id);
            if (applicationUser == null)
            {
                return NotFound();
            }
            return View(applicationUser);
        }

        // POST: ApplicationUsers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Firstname,MiddleName,Lastname,Id,UserName,Email")] ApplicationUser applicationUser)
        {
            if (id != applicationUser.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //_context.Update(applicationUser);
                    //await _context.SaveChangesAsync();

                    var user = await _userManager.FindByIdAsync(applicationUser.Id.ToString());

                    if (user == null)
                    {
                        Alert("Error!", "Failed to update. Application user cannot be found.", AlertType.error);
                        return View(applicationUser);
                    }

                    user.Email = applicationUser.Email;
                    user.Firstname = applicationUser.Firstname;
                    user.Lastname = applicationUser.Lastname;
                    user.MiddleName = applicationUser.MiddleName;
                    user.UserName = applicationUser.UserName;
                    user.SecurityStamp = Guid.NewGuid().ToString("D");

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        Alert("Success!", "User information updated.", AlertType.success);
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        var errors = string.Join(" ", result.Errors.ToList().AsEnumerable().Select(e => e.Description)).Replace("'", string.Empty);
                        Alert("Error!", "Failed to update. An error updating details encountered. " + errors, AlertType.error);
                    }
                }
                catch (DbUpdateConcurrencyException dcx)
                {
                    if (!ApplicationUserExists(applicationUser.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        Alert("Error!", "Failed to update. Please contact your system adminsitrator. " + dcx.Message, AlertType.error);
                        return RedirectToAction(nameof(Index));
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(applicationUser);
        }

        // GET: ApplicationUsers/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // POST: ApplicationUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var applicationUser = await _context.Users.FindAsync(id);

            if (applicationUser == null)
            {
                Alert("Error!", "User record cannot be found.", AlertType.error);
                return RedirectToAction(nameof(Index));
            }

            //_context.Users.Remove(applicationUser);
            //await _context.SaveChangesAsync();

            try
            {
                var result = await _userManager.DeleteAsync(applicationUser);
                if (result.Succeeded)
                {
                    Alert("Success!", "User record removed.", AlertType.success);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateConcurrencyException dcx)
            {
                if (!ApplicationUserExists(applicationUser.Id))
                {
                    Alert("Error!", "User record cannot be found.", AlertType.error);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    Alert("Error!", string.Concat("Contact your system administrator. Exception message: ", dcx.Message), AlertType.error);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException)
            {
                Alert("Error!", string.Concat("Unhandled error occured, please contact your system administrator."), AlertType.error);
                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Index));
        }

        private bool ApplicationUserExists(Guid id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
