﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using static Sdrm.Web.Enums.Notification;

namespace Sdrm.Web.Controllers
{
    public class BaseController : Controller
    {
        public void Alert(string heading, string message, AlertType alertType)
        {
            var msg = "<script language='javascript'>swal('" + heading + "', '" + message + "','" + alertType + "')" + "</script>";
            TempData["notification"] = msg;
        }

        /// <summary>
        /// Sets the information for the system notification.
        /// </summary>
        /// <param name="message">The message to display to the user.</param>
        /// <param name="notifyType">The type of notification to display to the user: Success, Error or Warning.</param>
        public void Message(string message, AlertType notifyType)
        {
            TempData["Notification2"] = message;

            switch (notifyType)
            {
                case AlertType.success:
                    TempData["NotificationCSS"] = "alert-box success";
                    break;
                case AlertType.error:
                    TempData["NotificationCSS"] = "alert-box errors";
                    break;
                case AlertType.warning:
                    TempData["NotificationCSS"] = "alert-box warning";
                    break;

                case AlertType.info:
                    TempData["NotificationCSS"] = "alert-box notice";
                    break;
            }
        }
    }
}
