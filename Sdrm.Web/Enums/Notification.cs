﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sdrm.Web.Enums
{
    public class Notification
    {
        public enum AlertType
        {
            error,
            success,
            warning,
            info
        }
    }
}
