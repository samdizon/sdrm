﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sdrm.Core.Entities
{
    public class Location
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
    }
}
