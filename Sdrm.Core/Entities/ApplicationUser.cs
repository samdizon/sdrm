﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sdrm.Core.Entities
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        [StringLength(50)]
        public string Firstname { get; set; }
        
        [StringLength(20)]
        public string MiddleName { get; set; }
        
        [StringLength(50)]
        public string Lastname { get; set; }
    }
}
