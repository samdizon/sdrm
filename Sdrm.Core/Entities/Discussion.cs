﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Sdrm.Core.Entities
{
    public class Discussion
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Subject { get; set; }

        [ForeignKey("Location")]
        public int LocationId { get; set; }
        public virtual Location Location { get; set; }

        [MaxLength(1000)]
        public string Summary { get; set; }
    }
}
