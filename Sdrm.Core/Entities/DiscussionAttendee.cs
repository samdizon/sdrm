﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Sdrm.Core.Entities
{
    public class DiscussionAttendee
    {
        public int Id { get; set; }

        public int DiscussionId { get; set; }

        [ForeignKey("Attendee")]
        public Guid AttendeeId { get; set; }
        public virtual ApplicationUser Attendee { get; set; }
    }
}
