﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sdrm.Core.Entities
{
    public class DiscussionOutcome
    {
        public int Id { get; set; }

        public int DiscussionId { get; set; }
    }
}
